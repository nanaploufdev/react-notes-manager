import s from "./style.module.css";
import {useNavigate, useParams, useSearchParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {NoteForm} from "../../components/NoteForm/NoteForm";
import {useState} from "react";
import {NoteAPI} from "../../api/note-api";
import {deleteNote, updateNote} from "../../store/note/note-slice";

export function Note(props) {
    const [isEditable, setIsEditable] = useState(false);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { id } = useParams();
    const note = useSelector((store) =>
        store.NOTE.noteList.find((note) => note.id === id)
    );

    function deleteNote_(note){
        if(window.confirm("Do you ok delete the note ?")){
            NoteAPI.deleteById(note.id);
            dispatch(deleteNote(note));
            navigate("/");
        }
    }

    async function submit(formValues) {
        const updatedNote = await NoteAPI.update({ ...formValues, id: id });
        dispatch(updateNote(updatedNote));
        setIsEditable(false);
    }
    return (
        <>
            {note && (
                <NoteForm
                    isEditable={isEditable}
                    title={isEditable ? "Edit note" : note.title}
                    note={note}
                    onClickEdit={() => setIsEditable(!isEditable)}
                    onSubmit={isEditable && submit}
                    onClickTrash={()=>{deleteNote_(note)}}
                />
            )}
        </>
    );
}