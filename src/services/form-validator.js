export class ValidatorService {
    static min(value, min) {
        if (value.length < min) {
            if( min === 1){
                return `Veuillez tapper au moins ${min} lettre`;
            }
            return `Veuillez tapper au moins ${min} lettres`;
        }
    }

    static max(value, max) {
        if (value.length > max) {
            if( max === 1){
                return `Veuillez tapper au plus ${max} lettre`;
            }
            return `Veuillez tapper au plus ${max} lettres`;
        }
    }
}