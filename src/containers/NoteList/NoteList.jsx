import s from "./style.module.css";
import {TextCard} from "../../components/TextCard/TextCard";
import {useDispatch, useSelector} from "react-redux";
import {store} from "../../store";
import {useNavigate} from "react-router-dom";
import {NoteAPI} from "../../api/note-api";
import {deleteNote} from "../../store/note/note-slice";

export function NoteList(props) {

    const noteList = useSelector((store) => store.NOTE.noteList);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    function deleteNote_(note){
        if(window.confirm("Do you ok delete the note ?")){
            NoteAPI.deleteById(note.id)
            dispatch(deleteNote(note))
        }
    }

    return(
        <div className="row justify-content-center">
            {noteList.map((note) =>{
                return(
                    <div key={note.id} className={s.card_container}>
                        <TextCard
                            title={note.title}
                            subtitle={note.subtitle}
                            content={note.content}
                            onClick={()=> navigate("/note/"+note.id)}
                            onClickTrash={()=>{deleteNote_(note)}}
                        />
                    </div>
                )
            })}

    </div>
    )
}