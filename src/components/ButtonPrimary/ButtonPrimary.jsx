import s from "./style.module.css";

export function ButtonPrimary({children, onClick, isDisabled}){
    return(
        <button disabled={isDisabled} onClick={onClick} type="button" className={`btn ${s.button}`}>
            {children}
        </button>
    );
}