import s from "./style.module.css";
import { Trash as TrashIcon } from "react-bootstrap-icons";
import { useState } from "react";
export function TextCard({title, subtitle, content, onClick, onClickTrash}){
    const [isCardHovered, setIsCardHovered] = useState(false);
    const [isTrashHovered, setIsTrashHovered] = useState(false);

    function onClickTrash_(e) {
        onClickTrash();
        e.stopPropagation();
    }
    return (
        <div
            onClick={onClick}
            onMouseEnter={() => setIsCardHovered(true)}
            onMouseLeave={() => setIsCardHovered(false)}
            className={`card ${s.container}`}
            style={{ borderColor: isCardHovered ? "#ec52b6" : "transparent" }}
        >
            <div className="card-body">
                <div className={s.title_row}>
                    <h5 className="card-title">{title}</h5>
                    <TrashIcon
                        size={20}
                        onMouseEnter={() => setIsTrashHovered(true)}
                        onMouseLeave={() => setIsTrashHovered(false)}
                        style={{ color: isTrashHovered ? "#b20215" : "#b8b8b8" }}
                        onClick={onClickTrash_}
                    />
                </div>
                <h6 className="card-subtitle mb-2 text-muted">{subtitle}</h6>
                <p className={`card-text ${s.text_content}`}>{content}</p>
            </div>
        </div>
    );
}